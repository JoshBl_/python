# Regression is used when trying to find a relationship between variables
# we can use that relationship to predict the outcome of future events
# Linear regression uses the relationship between the data=points to draw a straight line (which is used to predict future values)

import matplotlib.pyplot as plt
# importing scipy to draw the line of Linear Regression
from scipy import stats

# these arrays represent the x and y axis
x = [5,7,8,7,2,17,2,9,4,11,12,9,6]
y = [99,86,87,88,111,86,103,87,94,78,77,85,86]

# this returns key values of Linear Regression
slope, intercept, r, p, std_err = stats.linregress(x, y)

# this function uses the slope and intercept to return a new value - which represents where on the y axis the x value will be placed
def drawX(x):
    return slope * x + intercept

# running each value from the x array through the drawX function, this will result in a new array with new values for the y axis
newModel = list(map(drawX, x))

# draw the scatter plot
plt.scatter(x, y)
# draws the line of linear regression
plt.plot(x, newModel)
# display
plt.show()

# R-Squared - the relationship is measured with a value
# this value ranges from 0 to 1. 0 equals no relationship, 1 equals to 100% related

# although this prints -0.76 - this does show a relationship and we can use linear regression for future predictions
print(round(r,2))

# predicting future values

value = drawX(25)

print(round(value,2))