# importing pyplot to draw the graph including the line of linear regression
import matplotlib.pyplot as plt
# importing scipy to draw the line of Linear Regression
from scipy import stats
# needed for making the X and Y axis numpy arrays
import numpy

# boolean values
# these booleans will be referred to as global variables in functions so the value can be changed
xAxisAdded = False
yAxisAdded = False
menu = True

# test data
#2000, 2001, 2002, 2003, 2004, 2005, 2006, 2007, 2008, 2009, 2010, 2011, 2012, 2013, 2014, 2015, 2016, 2017, 2018, 2019
#126282, 148630, 177948, 222582, 240869, 253564, 250250, 274463, 298417, 254852, 262020, 275860, 277554, 284119, 298446, 341733, 390376, 419366, 435652, 428394

# new arrays for the x and Y axis
yearX = []
valueY = []

# function to add values to the X axis
def addToXAxis():
    # global used so the boolean can be accessed inside and outside of a function
    global xAxisAdded
    while xAxisAdded == False:
        try:
            monthCount = 1
            print("Adding values to the X axis (year) - the X axis can hold only 20 values")
            while monthCount <= 20:
                print(f"Please enter the value ")
                year = input(">")
                # convert the input to an integer
                year = int(year)
                # add value to the array
                yearX.append(year)
                print(f"Added year {year}")
                # increment value by 1 when a value is added to the array
                monthCount += 1
            else:
                # once array is populated, set boolean to true, designed to prevent the user from adding more than 20 values to the array
                xAxisAdded = True
        # error handling
        except ValueError as e:
            print("Error! Please enter a valid number.")
            print(f"Error details: {e}")
            # clearing the array as a precaution
            yearX.clear()
            # break to return to the menu and to prevent the user from adding values to the array
            break
    else:
        print("X axis is now populated.\nIf you want to add new values, please clear the currently held values.")
        print("Press enter to return to the main menu.")
        enter = input("")

# function to clear the array for the X axis
def clearXAxis(yearX):
    # global used so the boolean can be accessed inside and outside of a function
    global xAxisAdded
    print("Now clearing all held values in the X axis.\nPlease wait...")
    # clear the array of all values currently held
    yearX.clear()
    # changing value of boolean - this will allow the user to add values to the array again
    xAxisAdded = False
    print("Values in the X axis have been cleared. You can now add new values to the X axis.\nPress enter to return to the main menu")
    enter = input("")

# function for displaying all values held in X axis array
def displayXAxis(yearX):
    print("Now displaying the values held in the X axis")
    print(yearX)
    print("\nPress enter to return to the main menu.")
    enter = input("")

# function to add values to the Y axis
def addtoYAxis():
    # global used so the boolean can be accessed inside and outside of a function
    global yAxisAdded
    while yAxisAdded == False:
        try:
            monthCount = 1
            print("Adding values to the Y axis (value) - the Y axis can hold only 20 values")
            while monthCount <= 20:
                print(f"Please enter the value ")
                value = input(">")
                # convert the input to an integer
                value = int(value)
                # add value to the array
                valueY.append(value)
                print(f"Added value {value}")
                # increment value by 1 when a value is added to the array
                monthCount += 1
            else:
                # once array is populated, set boolean to true, designed to prevent the user from adding more than 20 values to the array
                yAxisAdded = True
        except ValueError as e:
            print("Error! Please enter a valid number!")
            print(f"Error details: {e}")
            # clearing array as a precaution
            valueY.clear()
            # break to return to the menu and to prevent the user from adding values to the array
            break
    else:
        print("Y axis is now populated.\nIf you want to add new values, please clear the currently held values.")
        print("Press enter to continue.")
        enter = input("")

# function to clear the array for the Y axis
def clearYAxis(valueY):
    global yAxisAdded
    print("Now clearing all held values in the Y axis.\nPlease wait...")
    valueY.clear()
    # changing the value on the boolean - this will allow the user to add values to the array again
    yAxisAdded = False
    print("Values in the Y axis have been cleared. You can now add new values to the Y axis.\nPress enter to return to the main menu")
    enter = input("")

# function for displaying all values held in the Y axis
def displayYAxis(valueY):
    print("Now displaying the values held in the Y axis")
    print(valueY)
    print("\nPress enter to return to the main menu")
    enter = input("")

# function for plotting data on the graph and displaying it
def plotGraph(yearX, valueY):
    global xAxisAdded, yAxisAdded
    # check to ensure that both booleans are true
    if xAxisAdded == True & yAxisAdded == True:
        # key info for linear regression
        slope, intercept, rSquared, p, std_err = stats.linregress(yearX, valueY)
        # run through each value from the X axis via the drawX function
        def drawX(x):
            return slope * x + intercept
        print("Now drawing graph, please wait...")
        # calculating R-Squared value
        print(f"The relationship between the values on the X axis and the Y axis is {round(rSquared,2)}")
        # this creates a list object (list) and calculates the length of each word in the tuple (map)
        newModel = list(map(drawX, yearX))
        # draw the scatter plot
        plt.scatter(yearX, valueY)
        # draws the line of linear regression
        plt.plot(yearX, newModel)
        # title for the graph
        plt.title("Average House Prices")
        print("Close the graph to return to the program. Press enter to display the graph.")
        enter = input("")
        # display graph
        plt.show()
        # new boolean value
        predictBool = True
        while predictBool == True:
            print("Using the data available, would you like to predict house prices in a later year?\nType 'Y' for yes or 'N' for no.")
            userChoice = input(">")
            # converting the input to upper case
            userChoice = userChoice.upper()
            if userChoice == 'Y':
                try:
                    print("Please enter a year.")
                    futureYear = input("")
                    futureYear = int(futureYear)
                    prediction = drawX(futureYear)
                    print(f"In the year {futureYear}, the average price of a house will be £{round(prediction,0)}")
                    # calculating the percentage increase - accessing the last element in the X axis and Y axis array
                    print(f"That's a difference of {round(prediction / valueY[-1] * 100.0 - 100.0, 0)}% since {yearX[-1]}")
                    # change the value of the boolean to allow the while loop to end
                    predictBool = False
                    print("Press enter to return to the menu.")
                    enter = input("")
                # error handling
                except ValueError as e:
                    print("Error! Please enter a valid number!")
                    print(f"Error details: {e}")
            elif userChoice == 'N':
                predictBool = False
                print("Press enter to return to the main menu.")
                enter = input("")
            # if user enters something other than 'Y' or 'N' - print message
            else:
                print("Please enter a valid option.")
    else:
        # if either of the boolean values are false, print message
        print("Error! Please ensure that both the X axis and Y axis have values before plotting a graph.\nPress enter to return to the menu.")
        enter = input("")

# DIY switch statement
while menu == True:
    print("""Welcome to the House Price Calculator!
Type in the value against a menu option you want to run!
1) Add values to the X axis (year)
2) Add values to the Y axis (value)
3) Display values in X axis (year)
4) Display values in Y axis (value)
5) Plot graph (using values in X and Y axis)
6) Clear values in the X axis (year)
7) Clear values in the Y axis (value)
q) Quit""")
    menuChoice = input(">")
    if menuChoice == '1':
        addToXAxis()
    elif menuChoice == '2':
        addtoYAxis()
    elif menuChoice == '3':
        displayXAxis(yearX)
    elif menuChoice == '4':
        displayYAxis(valueY)
    elif menuChoice == '5':
        plotGraph(yearX, valueY)
    elif menuChoice == '6':
        clearXAxis(yearX)
    elif menuChoice == '7':
        clearYAxis(valueY)
    elif menuChoice == 'q':
        # terminates the program
        print("Goodbye!")
        menu = False
    # error handling for unknown inputs
    else:
        print("Error! Please try again!")
