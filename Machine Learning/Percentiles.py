# percentiles - used in statistics to give a number that describes the value that a given percent of the values are lower than
import numpy

ranges = [10, 65, 33, 30, 28, 46, 93, 74, 103, 28]

# what is the 75. percentile?
x = numpy.percentile(ranges, 75)

# prints 72 - meaning 75% of the values are 72 or less
print(round(x, 0))