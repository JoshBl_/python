# numpy needed for mean and median
import numpy
# scipy needed for mode
from scipy import stats

# mean - find the average value (take the sum of all values and divide the sum by the number of values)

range1 = [50, 55, 65, 24, 75, 22, 14, 58, 49, 30, 10, 55]

mean = numpy.mean(range1)

print(mean)

# median - the value in the middle, after it's been sorted
# if there are two values in the middle, add the numbers together and divide by 2
# i.e. (55 + 54) / 2

median = numpy.median(range1)

print(median)

#mode - the value that appears the most

mode = stats.mode(range1)

print(mode)