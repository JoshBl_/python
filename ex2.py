#a comment, this is so you can read your program!
#anything after the # is ignored by python

print("I could have code like this.") #and the comment is ignored

#you can use a comment to disable code
#print("This won't run")

print ("This will run.")