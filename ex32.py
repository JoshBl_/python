# building several lists
the_count = [1, 2, 3, 4, 5]
fruits = ['apples', 'oranges', 'pears', 'apricots']
change = [1, 'pennies', 2, 'dimes', 3, 'quarters']

# you can make 2-dimensional lists - [[1, 2, 3], [4, 5, 6]]

# iterate through a list
for number in the_count:
    print(f"This is count {number}")

# iterate through another list
for fruit in fruits:
    print(f"A fruit of type: {fruit}")

# can also go through mixed lists
for i in change:
    print(f"I got {i}")

# you can build lists, first start with an empty one
elements = []

# then use the range function to do 0 to 5 counts
for i in range(0, 6):
    #this will add numbers 0 - 5 to the elements list
    print(f"Adding {i} to the list.")
    # append is a function for lists
    elements.append(i)

# and you can print them out
for i in elements:
    print(f"Element was: {i}")