#defining more functions
def break_words(stuff):
    """This function will break up words for us."""
    # new variable called words, value is equal to passed in variable with split method called split
    words = stuff.split(' ')
    return words

def sort_words(words):
    """Sorts the words"""
    # sorts the words in ascending order
    return sorted(words)

def print_first_word(words):
    """Prints the first word after popping it off."""
    # finds the word at position 0 and assings it to word variable
    word = words.pop(0)
    print(word)

def print_last_word(words):
    """Prints the last word after popping it off."""
    # similar to above, but with -1 referring to the last word
    word = words.pop(-1)
    print(word)

def sort_sentence(sentence):
    """Takes in a full sentence and returns the sorted words."""
    # the value of words is the break_words method with the sentence argument passed in
    words = break_words(sentence)
    # the words variable is then passed in as a parameter for the sort_words function
    return sort_words(words)

def print_first_and_last(sentence):
    """Prints the first and last words of the sentence"""
    # break the sentence first
    words = break_words(sentence)
    # then print the first and last words
    print_first_word(words)
    print_last_word(words)

def print_first_and_last_sorted(sentence):
    """Sorts the words and then prints the first and last one"""
    # sort the sentence
    words = sort_sentence(sentence)
    # print the first and last words
    print_first_word(words)
    print_last_word(words)