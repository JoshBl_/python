#the end=' ' at the end of each print line so we tell it to NOT to end the line with a newline character and go to the next line
print("How old are you?", end=' ')
#input() is used to get input from the user
#you can have input(prompt) - which represents a default message before the input
#convert input into a integer
age = int(input())
print("How tall are you?", end=' ')
height = input()
print("How much do you weigh in kg?", end=' ')
weight = int(input())

print(f"So, you're {age} old, {height} tall and {weight} heavy.")
print(f"If we added your weight ({weight}) and your age ({age}) together...\nWe get {age + weight}")

print("What's your favourite food?")
food = input()

print(f"Ah! So you're favourite food is {food}!")