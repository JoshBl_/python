#printing
print("Let's practice everything.")
print("You\'d need to know \'bout escapes with \\ that do:")
print('\n newlines and \t tabs.')

#more printing where we can print as many lines we want
poem = """
\tThe lovely world
with logic so firmly planted
cannot discern \n the needs of love
nor comprehend passion from intuition
and requires an explanation
\n\twhere there is none.
"""
#calling the variable to print the poem
print("--------------")
print(poem)
print("--------------")

#new variable where the value is 5
five = 10 - 2 + 3 - 6
#printing the value of five
print(f"This should be five: {five}")

#new function defined
def secret_formula(started):
    #value is equal to argument times 500
    jelly_beans = started * 500
    #takes the values of jelly_beans divided by 1000
    jars = jelly_beans / 1000
    #takes the value of jars and and divides it by 100
    crates = jars / 100
    #return the values
    return jelly_beans, jars, crates

#new variable with a value of 10,000
start_point = 10000
#new variables, where the values are assigned from the return statement in the function
beans, jars, crates = secret_formula(start_point)

#another way to format a string
print("With a starting point of: {}".format(start_point))

#print the values of the variables
print(f"We'd have {beans} beans, {jars} jars, and {crates} crates.")
start_point = start_point / 10

#another way of printing the variables from the function
print("We can also do that this way:")
formula = secret_formula(start_point)
#an easy way to apply a list to a format string
print("We'd have {} beans, {} jars, and {} crates".format(*formula))