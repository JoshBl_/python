#import argv module from sys
from sys import argv

script, filename = argv

print(f"We're going to erase {filename}")
print("If you don't want that, hit CTRL-C")
print("If you do want that, hit RETURN")

input("?")

print("Opening the file...")
#open takes two parameters, the filename and then the mode
#r - read
#a - append
#w - write
#x - create
#if the file should be handled as binary or text mode
#t = text
#b - binary
target = open(filename, 'w')

print("Truncating the file. Goodbye!")
target.truncate()

print("Now I'm going to ask you for three lines.")

line1 = input("line 1: ")
line2 = input("line 2: ")
line3 = input("line 3: ")

print("I'm going to write these to the file!")

#write content to the specified file at the beginning
#remember - when we opened the file we gave the mode of write
target.write(line1)
target.write("\n")
target.write(line2)
target.write("\n")
target.write(line3)
target.write("\n")

#close the file
print("And finally, we close it.")
target.close()