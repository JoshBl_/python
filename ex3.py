#when a number is defined as 5 or 3 - it's a int
#when a number is defined as 4.0 or 2.0 - it's a float
print("I will now count my chickens:")

#add and then divide
print("Hens", 25.0 + 30.0 / 6.0)
#subtract then multiply and then modulus
print("Roosters", 100.0 - 25.0 * 3.0 % 4.0)

print("Now I will count the eggs:")

#long calculation
print(3.0 + 2.0 + 1.0 - 5.0 + 4.0 % 2.0 - 1.0 / 4.0 + 6.0)

print("Is it true that 3 + 2 < 5 - 7?")

#print false - we're using the less than operator here
print(3.0 + 2.0 < 5.0 - 7.0)

#add and subtract
print("What is 3 + 2?", 3.0 + 2.0)
print("What is 5 - 7?", 5.0 - 7.0)

print("Oh, that's why it's False!")

print("How about some more!")

#these statements will print true and false
print("Is it greater?", 5.0 > -2.0)
print("Is it greater or equal?", 5.0 >= -2.0)
print("Is it less or equal?", 5.0 <= -2.0)
